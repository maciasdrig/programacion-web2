<?php
                
/**
 * @author		Rodrigo Macias
 * @company 	Program web 2
 * @copyright 	2018
 * @version     1.0
 */
                
require_once "field.inc";
                     
class Structure_Rol
{

 	public $idrol;
 	public $hash;
 	public $rol;
 	public $estado; 
 
    function __construct()
    { 
 		$this->idrol = new Field("int", true);
 		$this->hash = new Field("varchar");
 		$this->rol = new Field("varchar");
 		$this->estado = new Field("enum"); 
	}
}
                
?>