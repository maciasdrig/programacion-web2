<?php
                
/**
 * @author		Rodrigo Macias
 * @company 	Program web 2
 * @copyright 	2018
 * @version     1.0
 */
                
require_once "field.inc";
require_once "Rol.inc";
                     
class Structure_Usuarios
{

 	public $idUse;
 	public $hash;
 	public $nombre;
 	public $usuario;
 	public $contrasena;
 	public $nacimiento;
 	public $avatar;
 	public $email;
 	public $sexo;
 	public $fechaReg;
 	public $verificado;
 	public $privada;
 	public $rol;
 	public $estado; 

	public $Rol;
 
    function __construct()
    { 
 		$this->idUse = new Field("int", true);
 		$this->hash = new Field("varchar");
 		$this->nombre = new Field("varchar");
 		$this->usuario = new Field("varchar");
 		$this->contrasena = new Field("varchar");
 		$this->nacimiento = new Field("date");
 		$this->avatar = new Field("varchar");
 		$this->email = new Field("varchar");
 		$this->sexo = new Field("varchar");
 		$this->fechaReg = new Field("datetime");
 		$this->verificado = new Field("int");
 		$this->privada = new Field("int");
 		$this->rol = new Field("int");
 		$this->estado = new Field("enum"); 

		$this->Rol = new Structure_Rol;
	}
}
                
?>