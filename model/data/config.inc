<?php

/**
 * @author		Miguel Macias
 * @company 	Infocal
 * @copyright 	2018
 * @version     1.0
 */

$key    = "RDR1G";
$sKey   = sha1($key);

define("SecurityKey", $sKey);

?>