<?php
                
/**
 * @author		Rodrigo Macias
 * @company 	Program web 2
 * @copyright 	2018
 * @version     1.0
 */
                
require_once "field.inc";
require_once "Marca.inc";
require_once "Modelo.inc";
                     
class Structure_Producto
{

 	public $idProducto;
 	public $hash;
 	public $descripcion;
 	public $precio;
 	public $idMarca;
 	public $idModelo;
 	public $estado; 

	public $Marca;
	public $Modelo;
 
    function __construct()
    { 
 		$this->idProducto = new Field("int", true);
 		$this->hash = new Field("varchar");
 		$this->descripcion = new Field("varchar");
 		$this->precio = new Field("decimal");
 		$this->idMarca = new Field("int");
 		$this->idModelo = new Field("int");
 		$this->estado = new Field("enum"); 

		$this->Marca = new Structure_Marca;
		$this->Modelo = new Structure_Modelo;
	}
}
                
?>