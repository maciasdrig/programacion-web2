<?php
                
/**
 * @author		Rodrigo Macias
 * @company 	Program web 2
 * @copyright 	2018
 * @version     1.0
 */
                
require_once "field.inc";
                     
class Structure_Marca
{

 	public $idMarca;
 	public $hash;
 	public $nombre;
 	public $estado; 
 
    function __construct()
    { 
 		$this->idMarca = new Field("int", true);
 		$this->hash = new Field("varchar");
 		$this->nombre = new Field("varchar");
 		$this->estado = new Field("enum"); 
	}
}
                
?>