<?php
                
/**
 * @author		Rodrigo Macias
 * @company 	Program web 2
 * @copyright 	2018
 * @version     1.0
 */

require_once "data/db.inc";
require_once "data/Producto.inc";
require_once "data/Marca.inc";
require_once "data/Modelo.inc";
                     
class RN_Producto extends DataBase
{
    function __construct()
    {
        $this->Open();
    }
    
    /** 
     * @abstract Funci�n para obtener la lista de producto(s) 
     * @return Lista de Structure_Producto
     */
    function GetList()
    {
        $sql = "Select 
					t1.idProducto as t1_idProducto,
					t1.hash as t1_hash,
					t1.descripcion as t1_descripcion,
					t1.precio as t1_precio,
					t1.idMarca as t1_idMarca,
					t1.idModelo as t1_idModelo,
					t1.estado as t1_estado,
					t2.idMarca as t2_idMarca,
					t2.hash as t2_hash,
					t2.nombre as t2_nombre,
					t2.estado as t2_estado,
					t3.idModelo as t3_idModelo,
					t3.hash as t3_hash,
					t3.nombre as t3_nombre,
					t3.estado as t3_estado
				from producto as t1
				inner join marca as t2
					 on t1.idMarca = t2.idMarca
				inner join modelo as t3
					 on t1.idModelo = t3.idModelo
				where t1.estado = 'Activo'";
        $res = $this->Execute($sql);
        
        $list = array();
        if ($this->ContainsData($res)){
            $data = $this->DataListStructure($res);
            foreach($data as $item)
            {
                $osProducto = new Structure_Producto;

 				$osProducto->idProducto->SetValue($item["t1_idProducto"]);
 				$osProducto->hash->SetValue($item["t1_hash"]);
 				$osProducto->descripcion->SetValue($item["t1_descripcion"]);
 				$osProducto->precio->SetValue($item["t1_precio"]);
 				$osProducto->idMarca->SetValue($item["t1_idMarca"]);
 				$osProducto->idModelo->SetValue($item["t1_idModelo"]);
 				$osProducto->estado->SetValue($item["t1_estado"]);

					$osMarca = new Structure_Marca;

 					$osMarca->idMarca->SetValue($item["t2_idMarca"]);
 					$osMarca->hash->SetValue($item["t2_hash"]);
 					$osMarca->nombre->SetValue($item["t2_nombre"]);
 					$osMarca->estado->SetValue($item["t2_estado"]);

					$osModelo = new Structure_Modelo;

 					$osModelo->idModelo->SetValue($item["t3_idModelo"]);
 					$osModelo->hash->SetValue($item["t3_hash"]);
 					$osModelo->nombre->SetValue($item["t3_nombre"]);
 					$osModelo->estado->SetValue($item["t3_estado"]);


				$osProducto->Marca = $osMarca;
				$osProducto->Modelo = $osModelo;

 				$list[] = $osProducto;                
            }            
        }
        
        return $list;
    }
    
    /** 
     * @abstract Funci�n para obtener los Datos de producto(s)
     * @param string hash
     * @return Structure_Producto
     */
    function GetData($_hash)
    {
        $sql = "Select 
					t1.idProducto as t1_idProducto,
					t1.hash as t1_hash,
					t1.descripcion as t1_descripcion,
					t1.precio as t1_precio,
					t1.idMarca as t1_idMarca,
					t1.idModelo as t1_idModelo,
					t1.estado as t1_estado,
					t2.idMarca as t2_idMarca,
					t2.hash as t2_hash,
					t2.nombre as t2_nombre,
					t2.estado as t2_estado,
					t3.idModelo as t3_idModelo,
					t3.hash as t3_hash,
					t3.nombre as t3_nombre,
					t3.estado as t3_estado
				from producto as t1
				inner join marca as t2
					 on t1.idMarca = t2.idMarca
				inner join modelo as t3
					 on t1.idModelo = t3.idModelo
				where t1.hash = '". $_hash . "'";
        $res = $this->Execute($sql);
        
        $osProducto = new Structure_Producto;
        
        if ($this->ContainsData($res)){
            $data = $this->DataListStructure($res);            
            
            foreach($data as $item)
            {
 				$osProducto->idProducto->SetValue($item["t1_idProducto"]);
 				$osProducto->hash->SetValue($item["t1_hash"]);
 				$osProducto->descripcion->SetValue($item["t1_descripcion"]);
 				$osProducto->precio->SetValue($item["t1_precio"]);
 				$osProducto->idMarca->SetValue($item["t1_idMarca"]);
 				$osProducto->idModelo->SetValue($item["t1_idModelo"]);
 				$osProducto->estado->SetValue($item["t1_estado"]);

					$osMarca = new Structure_Marca;

 					$osMarca->idMarca->SetValue($item["t2_idMarca"]);
 					$osMarca->hash->SetValue($item["t2_hash"]);
 					$osMarca->nombre->SetValue($item["t2_nombre"]);
 					$osMarca->estado->SetValue($item["t2_estado"]);

					$osModelo = new Structure_Modelo;

 					$osModelo->idModelo->SetValue($item["t3_idModelo"]);
 					$osModelo->hash->SetValue($item["t3_hash"]);
 					$osModelo->nombre->SetValue($item["t3_nombre"]);
 					$osModelo->estado->SetValue($item["t3_estado"]);


				$osProducto->Marca = $osMarca;
				$osProducto->Modelo = $osModelo;
            }            
        }
        
        return $osProducto;
    }
    
    /** 
     * @abstract Funci�n para guardar producto
     * @param Structure_Producto osProducto
     * @return bool
     */
    function Save($_osProducto)
    {
        $sql = "Insert into producto values (
				" . $_osProducto->idProducto->GetValue() . ",
				'" . $_osProducto->hash->GetValue() . "',
				'" . $_osProducto->descripcion->GetValue() . "',
				" . $_osProducto->precio->GetValue() . ",
				" . $_osProducto->idMarca->GetValue() . ",
				" . $_osProducto->idModelo->GetValue() . ",
				'" . $_osProducto->estado->GetValue() . "')";

        $res = $this->Execute($sql);

		$id   = $this->GetLastIdAutoGenerated();
		$hash = sha1($id);
		$sql2 = "Update producto set hash = '". $hash ."' where idProducto = " . $id;
		$res2 = $this->Execute($sql2);
        
        $r = ($res and $res2) ? true : false;
		return $r;
    }
    
    /** 
     * @abstract Funci�n para actualizar producto
     * @param Structure_Producto osProducto
     * @return bool
     */
    function Update($_osProducto)
    {
        $sql = "Update producto set 
					descripcion = '" . $_osProducto->descripcion->GetValue() . "',
					precio = " . $_osProducto->precio->GetValue() . ",
					idMarca = " . $_osProducto->idMarca->GetValue() . ",
					idModelo = " . $_osProducto->idModelo->GetValue() . ",
					estado = '" . $_osProducto->estado->GetValue() . "'
				where hash = '" . $_osProducto->hash->GetValue() . "'";
        $res = $this->Execute($sql);
        
        return $res;
    }
    
    /** 
     * @abstract Funci�n para eliminar producto
     * @param string hash
     * @return bool
     */
    function Delete($_hash)
    {
        $sql = "Update producto set estado = 'Inactivo' where hash = '" . $_hash . "'";
        $res = $this->Execute($sql);
        
        return $res;
    }
}
                
?>