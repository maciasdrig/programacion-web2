<?php
                
/**
 * @author		Rodrigo Macias
 * @company 	Program web 2
 * @copyright 	2018
 * @version     1.0
 */

require_once "data/db.inc";
require_once "data/Rol.inc";
                     
class RN_Rol extends DataBase
{
    function __construct()
    {
        $this->Open();
    }
    
    /** 
     * @abstract Funci�n para obtener la lista de rol(s) 
     * @return Lista de Structure_Rol
     */
    function GetList()
    {
        $sql = "Select * from rol where estado = 'Activo'";
        $res = $this->Execute($sql);
        
        $list = array();
        if ($this->ContainsData($res)){
            $data = $this->DataListStructure($res);
            foreach($data as $item)
            {
                $osRol = new Structure_Rol;

 				$osRol->idrol->SetValue($item["idrol"]);
 				$osRol->hash->SetValue($item["hash"]);
 				$osRol->rol->SetValue($item["rol"]);
 				$osRol->estado->SetValue($item["estado"]);

 				$list[] = $osRol;                
            }            
        }
        
        return $list;
    }
    
    /** 
     * @abstract Funci�n para obtener los Datos de rol(s)
     * @param string hash
     * @return Structure_Rol
     */
    function GetData($_hash)
    {
        $sql = "Select * from rol where idrol = '". $_hash . "'";
        $res = $this->Execute($sql);
        
        $osRol = new Structure_Rol;
        
        if ($this->ContainsData($res)){
            $data = $this->DataListStructure($res);            
            
            foreach($data as $item)
            {
 				$osRol->idrol->SetValue($item["idrol"]);
 				$osRol->hash->SetValue($item["hash"]);
 				$osRol->rol->SetValue($item["rol"]);
 				$osRol->estado->SetValue($item["estado"]);
            }            
        }
        
        return $osRol;
    }
    
    /** 
     * @abstract Funci�n para guardar rol
     * @param Structure_Rol osRol
     * @return bool
     */
    function Save($_osRol)
    {
        $sql = "Insert into rol values (
				" . $_osRol->idrol->GetValue() . ",
				'" . $_osRol->hash->GetValue() . "',
				'" . $_osRol->rol->GetValue() . "',
				'" . $_osRol->estado->GetValue() . "')";

        $res = $this->Execute($sql);

		$id   = $this->GetLastIdAutoGenerated();
		$hash = sha1($id);
		$sql2 = "Update rol set hash = '". $hash ."' where idrol = " . $id;
		$res2 = $this->Execute($sql2);
        
        $r = ($res and $res2) ? true : false;
		return $r;
    }
    
    /** 
     * @abstract Funci�n para actualizar rol
     * @param Structure_Rol osRol
     * @return bool
     */
    function Update($_osRol)
    {
        $sql = "Update rol set 
					rol = '" . $_osRol->rol->GetValue() . "',
					estado = '" . $_osRol->estado->GetValue() . "'
				where hash = '" . $_osRol->hash->GetValue() . "'";
        $res = $this->Execute($sql);
        
        return $res;
    }
    
    /** 
     * @abstract Funci�n para eliminar rol
     * @param string hash
     * @return bool
     */
    function Delete($_hash)
    {
        $sql = "Update rol set estado = 'Inactivo' where hash = '" . $_hash . "'";
        $res = $this->Execute($sql);
        
        return $res;
    }
}
                
?>