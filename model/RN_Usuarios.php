<?php
                
/**
 * @author		Rodrigo Macias
 * @company 	Program web 2
 * @copyright 	2018
 * @version     1.0
 */

require_once "data/db.inc";
require_once "data/Usuarios.inc";
require_once "data/Rol.inc";
                     
class RN_Usuarios extends DataBase
{
    function __construct()
    {
        $this->Open();
    }
    
    /** 
     * @abstract Funci�n para obtener la lista de usuarios(s) 
     * @return Lista de Structure_Usuarios
     */
    function GetList()
    {
        $sql = "Select 
					t1.id_use as t1_id_use,
					t1.hash as t1_hash,
					t1.nombre as t1_nombre,
					t1.usuario as t1_usuario,
					t1.contrasena as t1_contrasena,
					t1.nacimiento as t1_nacimiento,
					t1.avatar as t1_avatar,
					t1.email as t1_email,
					t1.sexo as t1_sexo,
					t1.fecha_reg as t1_fecha_reg,
					t1.verificado as t1_verificado,
					t1.privada as t1_privada,
					t1.rol as t1_rol,
					t1.estado as t1_estado,
					t2.idrol as t2_idrol,
					t2.hash as t2_hash,
					t2.rol as t2_rol,
					t2.estado as t2_estado
				from usuarios as t1
				inner join rol as t2
					 on t1.rol = t2.idrol
				where t1.estado = 'Activo'";
        $res = $this->Execute($sql);
        
        $list = array();
        if ($this->ContainsData($res)){
            $data = $this->DataListStructure($res);
            foreach($data as $item)
            {
                $osUsuarios = new Structure_Usuarios;

 				$osUsuarios->idUse->SetValue($item["t1_id_use"]);
 				$osUsuarios->hash->SetValue($item["t1_hash"]);
 				$osUsuarios->nombre->SetValue($item["t1_nombre"]);
 				$osUsuarios->usuario->SetValue($item["t1_usuario"]);
 				$osUsuarios->contrasena->SetValue($item["t1_contrasena"]);
 				$osUsuarios->nacimiento->SetValue($item["t1_nacimiento"]);
 				$osUsuarios->avatar->SetValue($item["t1_avatar"]);
 				$osUsuarios->email->SetValue($item["t1_email"]);
 				$osUsuarios->sexo->SetValue($item["t1_sexo"]);
 				$osUsuarios->fechaReg->SetValue($item["t1_fecha_reg"]);
 				$osUsuarios->verificado->SetValue($item["t1_verificado"]);
 				$osUsuarios->privada->SetValue($item["t1_privada"]);
 				$osUsuarios->rol->SetValue($item["t1_rol"]);
 				$osUsuarios->estado->SetValue($item["t1_estado"]);

					$osRol = new Structure_Rol;

 					$osRol->idrol->SetValue($item["t2_idrol"]);
 					$osRol->hash->SetValue($item["t2_hash"]);
 					$osRol->rol->SetValue($item["t2_rol"]);
 					$osRol->estado->SetValue($item["t2_estado"]);


				$osUsuarios->Rol = $osRol;

 				$list[] = $osUsuarios;                
            }            
        }
        
        return $list;
    }
    
    /** 
     * @abstract Funci�n para obtener los Datos de usuarios(s)
     * @param string hash
     * @return Structure_Usuarios
     */
    function GetData($_hash)
    {
        $sql = "Select 
					t1.id_use as t1_id_use,
					t1.hash as t1_hash,
					t1.nombre as t1_nombre,
					t1.usuario as t1_usuario,
					t1.contrasena as t1_contrasena,
					t1.nacimiento as t1_nacimiento,
					t1.avatar as t1_avatar,
					t1.email as t1_email,
					t1.sexo as t1_sexo,
					t1.fecha_reg as t1_fecha_reg,
					t1.verificado as t1_verificado,
					t1.privada as t1_privada,
					t1.rol as t1_rol,
					t1.estado as t1_estado,
					t2.idrol as t2_idrol,
					t2.hash as t2_hash,
					t2.rol as t2_rol,
					t2.estado as t2_estado
				from usuarios as t1
				inner join rol as t2
					 on t1.rol = t2.idrol
				where t1.id_use = '". $_hash . "'";
        $res = $this->Execute($sql);
        
        $osUsuarios = new Structure_Usuarios;
        
        if ($this->ContainsData($res)){
            $data = $this->DataListStructure($res);            
            
            foreach($data as $item)
            {
 				$osUsuarios->idUse->SetValue($item["t1_id_use"]);
 				$osUsuarios->hash->SetValue($item["t1_hash"]);
 				$osUsuarios->nombre->SetValue($item["t1_nombre"]);
 				$osUsuarios->usuario->SetValue($item["t1_usuario"]);
 				$osUsuarios->contrasena->SetValue($item["t1_contrasena"]);
 				$osUsuarios->nacimiento->SetValue($item["t1_nacimiento"]);
 				$osUsuarios->avatar->SetValue($item["t1_avatar"]);
 				$osUsuarios->email->SetValue($item["t1_email"]);
 				$osUsuarios->sexo->SetValue($item["t1_sexo"]);
 				$osUsuarios->fechaReg->SetValue($item["t1_fecha_reg"]);
 				$osUsuarios->verificado->SetValue($item["t1_verificado"]);
 				$osUsuarios->privada->SetValue($item["t1_privada"]);
 				$osUsuarios->rol->SetValue($item["t1_rol"]);
 				$osUsuarios->estado->SetValue($item["t1_estado"]);

					$osRol = new Structure_Rol;

 					$osRol->idrol->SetValue($item["t2_idrol"]);
 					$osRol->hash->SetValue($item["t2_hash"]);
 					$osRol->rol->SetValue($item["t2_rol"]);
 					$osRol->estado->SetValue($item["t2_estado"]);


				$osUsuarios->Rol = $osRol;
            }            
        }
        
        return $osUsuarios;
    }
    
    /** 
     * @abstract Funci�n para guardar usuarios
     * @param Structure_Usuarios osUsuarios
     * @return bool
     */
    function Save($_osUsuarios)
    {
        $sql = "Insert into usuarios values (
				" . $_osUsuarios->idUse->GetValue() . ",
				'" . $_osUsuarios->hash->GetValue() . "',
				'" . $_osUsuarios->nombre->GetValue() . "',
				'" . $_osUsuarios->usuario->GetValue() . "',
				'" . $_osUsuarios->contrasena->GetValue() . "',
				'" . $_osUsuarios->nacimiento->GetValue() . "',
				'" . $_osUsuarios->avatar->GetValue() . "',
				'" . $_osUsuarios->email->GetValue() . "',
				'" . $_osUsuarios->sexo->GetValue() . "',
				'" . $_osUsuarios->fechaReg->GetValue() . "',
				" . $_osUsuarios->verificado->GetValue() . ",
				" . $_osUsuarios->privada->GetValue() . ",
				" . $_osUsuarios->rol->GetValue() . ",
				'" . $_osUsuarios->estado->GetValue() . "')";

        $res = $this->Execute($sql);

		$id   = $this->GetLastIdAutoGenerated();
		$hash = sha1($id);
		$sql2 = "Update usuarios set hash = '". $hash ."' where id_use = " . $id;
		$res2 = $this->Execute($sql2);
        
        $r = ($res and $res2) ? true : false;
		return $r;
    }
    
    /** 
     * @abstract Funci�n para actualizar usuarios
     * @param Structure_Usuarios osUsuarios
     * @return bool
     */
    function Update($_osUsuarios)
    {
        $sql = "Update usuarios set 
					idUse = " . $_osUsuarios->idUse->GetValue() . ",,
					nombre = '" . $_osUsuarios->nombre->GetValue() . "',
					usuario = '" . $_osUsuarios->usuario->GetValue() . "',
					contrasena = '" . $_osUsuarios->contrasena->GetValue() . "',
					nacimiento = '" . $_osUsuarios->nacimiento->GetValue() . "',
					avatar = '" . $_osUsuarios->avatar->GetValue() . "',
					email = '" . $_osUsuarios->email->GetValue() . "',
					sexo = '" . $_osUsuarios->sexo->GetValue() . "',
					fechaReg = '" . $_osUsuarios->fechaReg->GetValue() . "',
					verificado = " . $_osUsuarios->verificado->GetValue() . ",
					privada = " . $_osUsuarios->privada->GetValue() . ",
					rol = " . $_osUsuarios->rol->GetValue() . ",
					estado = '" . $_osUsuarios->estado->GetValue() . "'
				where hash = '" . $_osUsuarios->hash->GetValue() . "'";
        $res = $this->Execute($sql);
        
        return $res;
    }
    
    /** 
     * @abstract Funci�n para eliminar usuarios
     * @param string hash
     * @return bool
     */
    function Delete($_hash)
    {
        $sql = "Update usuarios set estado = 'Inactivo' where hash = '" . $_hash . "'";
        $res = $this->Execute($sql);
        
        return $res;
    }
}
                
?>