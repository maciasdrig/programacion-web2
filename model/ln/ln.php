<?php

/**
 * @author		Miguel Macias
 * @copyright	2018
 * @company		The Avengers
 * @version		0.1
 */

$listLn = array(
    array("Code" => "11", "es" => "Listar", "en" => "List", "por" => "Listar"),
    
    array("Code" => "12", "es" => "Adicionar", "en" => "Add", "por" => "Adicionar"),
    array("Code" => "13", "es" => "Editar", "en" => "Edit", "por" => "Edite"),
    array("Code" => "14", "es" => "Eliminar", "en" => "Delete", "por" => "Eliminar"),
    array("Code" => "15", "es" => "Guardar", "en" => "Save", "por" => "Salvar"),
    array("Code" => "16", "es" => "Buscar", "en" => "Search", "por" => "Achar"),
    array("Code" => "17", "es" => "Version 1.0", "en" => "--------", "por" => "---------"),
    array("Code" => "18", "es" => "Ayuda", "en" => "-------", "por" => "---------"),
    
    array("Code" => "101", "es" => "Moneda", "en" => "Money", "por" => "Moeda"),
    array("Code" => "102", "es" => "Tipo Cuenta", "en" => "Acount Type", "por" => "Tipo de Conta"),
    array("Code" => "103", "es" => "Persona", "en" => "Person", "por" => "Pessoa"),
    array("Code" => "104", "es" => "Cuenta", "en" => "Acount", "por" => "Conta"),
    array("Code" => "105", "es" => "Producto", "en" => "Product", "por" => "Produto"),
    
    array("Code" => "106", "es" => "Monto", "en" => "Rode", "por" => "Quantidade"),
    array("Code" => "107", "es" => "Marca", "en" => "Brand", "por" => "Marca"),
    array("Code" => "108", "es" => "Tipo Tarjeta", "en" => "Card Type", "por" => "Tipo de Cartão"),
    array("Code" => "109", "es" => "Tarjeta", "en" => "Card", "por" => "Cartão"),
    array("Code" => "110", "es" => "Funcionario", "en" => "--------", "por" => "--------"),
    array("Code" => "111", "es" => "Usuario Interno", "en" => "--------", "por" => "--------"),
    array("Code" => "1111", "es" => "Operaciones", "en" => "---------", "por" => "--------"),
    array("Code" => "1112", "es" => "Fecha", "en" => "---------", "por" => "--------"),
    array("Code" => "1113", "es" => "Hora", "en" => "---------", "por" => "--------"),
    
    array("Code" => "200", "es" => "Lista de Monedas", "en" => "Moneys List", "por" => "Lista de Moedas"),
    array("Code" => "201", "es" => "Nro", "en" => "#", "por" => "Nro"),
    array("Code" => "202", "es" => "Nombre", "en" => "Name", "por" => "Nome"),
    array("Code" => "203", "es" => "Abreviatura", "en" => "Abreviature", "por" => "Abreviatura"),
    array("Code" => "204", "es" => "Acciones", "en" => "Actions", "por" => "ações"),
    array("Code" => "205", "es" => "Nueva Moneda", "en" => "New Money", "por" => "Nova Moeda"),
    array("Code" => "206", "es" => "Dirección", "en" => "Address", "por" => "Direção"),
    array("Code" => "207", "es" => "Teléfono", "en" => "Phone", "por" => "Telefone"),
    array("Code" => "208", "es" => "Apellido Paterno", "en" => "Last Name", "por" => "Apelido Paterno"),
    array("Code" => "209", "es" => "Apellido Materno", "en" => "Last Name 2", "por" => "Sobrenome Materno"),
    array("Code" => "210", "es" => "Fecha Nacimiento", "en" => "Birthdate", "por" => "Data de Nascimento"),
    array("Code" => "211", "es" => "CI", "en" => "ID", "por" => "Cartão de Identidade"),
    array("Code" => "212", "es" => "Género", "en" => "Gender", "por" => "Gênero"),
    array("Code" => "213", "es" => "Lista de Personas", "en" => "Persons List", "por" => "Lista de Pessoas"),
    array("Code" => "214", "es" => "Lista de Personas Naturales", "en" => "List of Natural Persons", "por" => "Lista de Pessoas Singulares"),
    array("Code" => "215", "es" => "Lista de Personas Jurídicas", "en" => "List of Legal Persons", "por" => "Lista de Pessoas Colectivas"),
    array("Code" => "216", "es" => "Razón Social", "en" => "Business name", "por" => "Razão Social"),
    array("Code" => "217", "es" => "NIT", "en" => "NIT", "por" => "Nitido"),
    array("Code" => "218", "es" => "Nueva Persona Natural", "en" => "New Natural Person", "por" => "Nova Pessoa Natural"),
    array("Code" => "219", "es" => "Nueva Persona Jurídica", "en" => "New Legal Person", "por" => "Pessoa Juridica Nova"),
    array("Code" => "220", "es" => "Editar Persona Natural", "en" => "Edit Natural Person", "por" => "Editar Pessoa Natural"),
    array("Code" => "221", "es" => "Editar Persona Jurídica", "en" => "Edit Legal Person", "por" => "Editar Pessoa Juridica"),
    array("Code" => "222", "es" => "Lista de Sucursales", "en" => "Branch list", "por" => "Lista de Filiais"),
    array("Code" => "223", "es" => "Nueva Sucursal", "en" => "New Branch", "por" => "Novo Ramo"),
    array("Code" => "224", "es" => "Editar Sucursal", "en" => "Edit Branch", "por" => "Editar Ramo"),
    array("Code" => "225", "es" => "Sucursal", "en" => "Branch", "por" => "Ramo"),
    array("Code" => "226", "es" => "Usuario", "en" => "User", "por" => "Utilizador"),
    array("Code" => "227", "es" => "Lista de usuarios", "en" => "User List", "por" => "Lista de Usuários"),
    array("Code" => "228", "es" => "Usuario", "en" => "User", "por" => "Utilizador"),
    array("Code" => "229", "es" => "Contraseña", "en" => "Password", "por" => "Password"),
    array("Code" => "230", "es" => "Nuevo usuario", "en" => "New User", "por" => "Novo Usuário"),
    array("Code" => "231", "es" => "Editar usuario", "en" => "Edit User", "por" => "Editar Usuário"),
    array("Code" => "232", "es" => "Nueva Cuenta", "en" => "New Acount", "por" => "Nova Conta"),
    array("Code" => "233", "es" => "Editar Cuenta", "en" => "Edit Acount", "por" => "Editar Conta"),
    array("Code" => "234", "es" => "Nombre Marca", "en" => "Brand Name", "por" => "Nome da Marca"),
    array("Code" => "235", "es" => "Editar Marca", "en" => "Edit Brand", "por" => "Editar Marca"),
    array("Code" => "236", "es" => "Nro Cuenta", "en" => "Account Number", "por" => "Numero da Conta"),
    array("Code" => "237", "es" => "Lista de Marca", "en" => "Brand List", "por" => "Lista de Marcas"),
    
    array("Code" => "238", "es" => "Lista de Cuentas", "en" => "Account List", "por" => "Lista de Contas"),
    array("Code" => "239", "es" => "Nueva Marca", "en" => "New Brand", "por" => "Nova Marca"),
    array("Code" => "240", "es" => "Nuevo Tipo Cuenta", "en" => "New Account Type", "por" => "Novo Tipo de Conta"),
    array("Code" => "241", "es" => "Lista de Tipo Cuentas", "en" => "Account Type List", "por" => "Lista de Tipo de Conta"),
    array("Code" => "242", "es" => "Lista de Tipo Tarjeta", "en" => "Card Type List", "por" => "Lista de Tipo de Cartão"),
    array("Code" => "243", "es" => "Nuevo Tipo Tarjeta", "en" => "New Type Card", "por" => "Novo Cartão de Tipo"),
    array("Code" => "244", "es" => "Editar Tipo Tarjeta", "en" => "Edit Card Type", "por" => "Editar Tipo de Cartão"),
    array("Code" => "245", "es" => "Lista de Productos", "en" => "List of Product", "por" => "Lista de Produtos"),
    array("Code" => "246", "es" => "Nuevo Producto", "en" => "New Product", "por" => "Novo Produto"),
    array("Code" => "247", "es" => "Editar Producto", "en" => "Edit Product", "por" => "Editar Produto"),
    array("Code" => "248", "es" => "Lista de Tarjeta", "en" => "Card List", "por" => "lista de cartões"),
    
    
    array("Code" => "249", "es" => "Nueva Tarjeta", "en" => "New Card", "por" => "Novo Cartão"),
    array("Code" => "250", "es" => "Editar Tarjeta", "en" => "Edit Card", "por" => "Editar Cartão"),
    array("Code" => "251", "es" => "Nro de Tarjeta", "en" => "Card Number", "por" => "Numero de Cartão"),
    array("Code" => "252", "es" => "Cvs", "en" => "---------", "por" => "--------"),
    array("Code" => "253", "es" => "Mes de Vencimiento", "en" => "--------", "por" => "--------"),
    array("Code" => "254", "es" => "Año de Vencimiento", "en" => "--------", "por" => "--------"),
    array("Code" => "255", "es" => "Fecha Emision", "en" => "Broadcast date", "por" => "Data de Emissão"),
    array("Code" => "256", "es" => "Pin", "en" => "Pin", "por" => "Pin"),
    array("Code" => "257", "es" => "Lista de Funcionarios", "en" => "-----", "por" => "------"),
    array("Code" => "258", "es" => "Fecha Ingreso", "en" => "-----", "por" => "------"),
    array("Code" => "259", "es" => "Sueldo", "en" => "-----", "por" => "------"),
    array("Code" => "260", "es" => "Nuevo Funcionario", "en" => "-----", "por" => "------"),
    array("Code" => "261", "es" => "Editar Funcionario", "en" => "-----", "por" => "------"),
    
    array("Code" => "262", "es" => "Lista de Usuario Interno", "en" => "---------", "por" => "---------"),
    array("Code" => "263", "es" => "Nuevo Usuario Interno", "en" => "---------", "por" => "---------"),
    array("Code" => "264", "es" => "Editar Usuario Interno", "en" => "-----", "por" => "------"),
    array("Code" => "265", "es" => "Usuario", "en" => "-----", "por" => "------"),
    array("Code" => "266", "es" => "Contraceña", "en" => "-----", "por" => "------"),
    array("Code" => "267", "es" => "Fecha Actualizacion Contraceña", "en" => "-----", "por" => "------"),
    //array("Code" => "268", "es" => "", "en" => "-----", "por" => "------"),
    
)


?>