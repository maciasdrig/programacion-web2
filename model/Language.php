<?php

/**
 * @author www.intercambiosvirtuales.org
 * @copyright 2018
 */

class Language
{    
    public $listLn;
    
    function __construct()
    {
        require_once "ln/ln.php";
        
        $this->listLn = $listLn;
    }
    function Search($_code, $_ln)
    {
        foreach($this->listLn as $item){
            if ($_code == $item["Code"]){
                return $item[$_ln];
            }
        }
    }
    
    function All($_ln)
    {                
        $content = "";
        $i = 1;
        foreach($this->listLn as $item){
            if ($i > 1) $content .= "#";
            $content .= $item["Code"] . "," . $item[$_ln];
            $i++;
        }
        
        return $content;
    }
}

?>