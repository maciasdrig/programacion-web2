<?php

/**
 * @author		Rodrigo macias
 * @copyright	2018
 * @company		The Avengers
 * @version		0.1
 */
 

$mnu = ( isset($_GET["mnu"]) ) ? $_GET["mnu"] : "login";

switch($mnu){

	case "login":
		 require_once "control/c-index_.php";
		break;

    case "crearUsuario":
        $op = ( isset($_GET["op"]) ) ? base64_decode($_GET["op"]) : "list";
        
        switch($op){
            case "new":
                require_once "control/c-usuario-new.php";
                break;
            case "save":
                require_once "control/c-usuario-save.php";
                break;
            case "list":
                require_once "control/c-usuario-list.php";
                break;
            case "edit":
                require_once "control/c-usuario-edit.php";
                break;
            case "update";
                require_once "control/c-usuario-update.php";
                break;
            case "delete":
                require_once "control/c-usuario-delete.php";
                break;
        }
        
        break;
    case "tipocuenta":
        $op = ( isset($_GET["op"]) ) ?base64_decode($_GET["op"]): "list";
        
        switch($op){
            case "new":
                require_once "control/c-tipocuenta-new.php";
                break;
            case "save":
                require_once "control/c-tipocuenta-save.php";
                break;
            case "list":
                require_once "control/c-tipocuenta-list.php";
                break;
            case "edit":
                require_once "control/c-tipocuenta-edit.php";
                break;
            case "update";
                require_once "control/c-tipocuenta-update.php";
                break;
            case "delete":
                require_once "control/c-tipocuenta-delete.php";
                break;
        }
        break;

    case "marca":
        $op = ( isset($_GET["op"]) ) ? base64_decode($_GET["op"]): "list";
        
        switch($op){
            case "new":
                require_once "control/c-marca-new.php";
                break;
            case "save":
                require_once "control/c-marca-save.php";
                break;
            case "list":
                require_once "control/c-marca-list.php";
                break;
            case "edit":
                require_once "control/c-marca-edit.php";
                break;
            case "update";
                require_once "control/c-marca-update.php";
                break;
            case "delete":
                require_once "control/c-marca-delete.php";
                break;
        }
        break;
}

?>