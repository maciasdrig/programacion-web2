<?php

/**
 * @author		Miguel Macias
 * @copyright	2018
 * @company		The Avengers
 * @version		0.1
 */
session_start();
 
require_once "../../model/RN_Rol.php";
require_once "../../model/RN_Usuarios.php";
$u=$_SESSION['id_rol']; 
$s=$_SESSION['usuario']; 
$e=$_SESSION['id']; 
$oRN_Rol = new RN_Rol;
$listaRol = $oRN_Rol->GetData($u);
$oRN_Usuario = new RN_Usuarios;
$lista_Usuario = $oRN_Usuario->GetData($e);
$listaTipoTarjeta =$oRN_Usuario->GetList();

include_once "../view/v-registro_.php";

?>
