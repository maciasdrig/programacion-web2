<?php

/**
 * @author		Miguel Macias
 * @copyright	2018
 * @company		The Avengers
 * @version		0.1
 */

require_once "../../model/RN_Producto.php";

$osProducto = new Structure_Producto;

$osProducto->idProducto->SetValue(0);
$osProducto->hash->SetValue("");
$osProducto->descripcion->SetValue($_POST["Descripcion"]);
$osProducto->precio->SetValue($_POST["Precio"]);
$osProducto->idMarca->SetValue($_POST["op_M"]);
$osProducto->idModelo->SetValue($_POST["op_m"]);
$osProducto->estado->SetValue("Activo");

$oRN_Producto = new RN_Producto;

$res = $oRN_Producto->Save($osProducto);

if ($res){
    header("location: c-producto-list.php");
}else{
    echo "Err 100";
}

?>