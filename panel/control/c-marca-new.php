<?php

/**
 * @author		Miguel Macias
 * @copyright	2018
 * @company		The Avengers
 * @version		0.1
 */

session_start();
 
require_once "../../model/RN_Rol.php";
require_once "../../model/RN_Marca.php";
require_once "../../model/RN_Modelo.php";
require_once "../../model/RN_Usuarios.php";

$u=$_SESSION['id_rol']; 
$s=$_SESSION['usuario']; 
$e=$_SESSION['id'];

$oRN_Rol = new RN_Rol;
$listaRol = $oRN_Rol->GetData($u);
$oRN_Usuario = new RN_Usuarios;
$lista_Usuario = $oRN_Usuario->GetData($e);

$oRN_Marca = new RN_Marca;
$listaMarca =$oRN_Marca->GetList();
$oRN_Modelo = new RN_Modelo;
$listaModelo =$oRN_Modelo->GetList();



include_once "../view/v-marca-new.php";

?>