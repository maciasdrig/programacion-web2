<?php

/**
 * @author www.intercambiosvirtuales.org
 * @copyright 2018
 */

session_start();
require_once "../../model/RN_Marca.php";
require_once "../../model/RN_Modelo.php";
require_once "../../model/RN_Rol.php";
require_once "../../model/RN_Usuarios.php";
require_once"../../model/RN_Producto.php";
$u=$_SESSION['id_rol']; 
$s=$_SESSION['usuario']; 
$e=$_SESSION['id']; 

$oRN_Rol = new RN_Rol;
$listaRol = $oRN_Rol->GetData($u);
$oRN_Usuario = new RN_Usuarios;
$lista_Usuario = $oRN_Usuario->GetData($e);


$hash = $_GET["data"]; // hash producto 
$_SESSION["hash"] = $hash;

$oRN_Producto = new RN_Producto;

$data = $oRN_Producto->GetData($hash);

$oRN_Producto     = new RN_Producto;
$oRN_Marca           = new RN_Marca;
$oRN_Modelo          = new RN_Modelo;


$listaProducto       = $oRN_Producto->GetList();
$listaMarca             = $oRN_Marca->GetList();
$listaModelo           = $oRN_Modelo->GetList();

include_once "../view/v-producto-edit_.php";




?>