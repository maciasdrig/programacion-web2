<?php

/**
 * @author		Miguel Macias
 * @copyright	2018
 * @company		The Avengers
 * @version		0.1
 */

require_once "../../model/RN_Marca.php"; //ACCEDIENDO A LA RN_Marca


$Marca = $_POST["Marca"]; //recuperando los datos y almacenando en la variable $Marca


$osMarca = new  Structure_Marca;  //Instaciamos un structura de Marca
$osMarca->idMarca->SetValue(0);   //Accedidiendo a los campos de la estructura 
$osMarca->hash->SetValue(""); //Accedidiendo a los campos de la estructura 
$osMarca->nombre->SetValue($Marca);//Accedidiendo a los campos de la estructura 
$osMarca->estado->SetValue("Activo");//Accedidiendo a los campos de la estructura 
///y cargando datos a la estructura 
$os_Marca = new RN_Marca;  //Instaciamos un RN_Marca de Marca 
// es un objeto de la RN_Marca ---> $os_Marca 
$res = $os_Marca->Save($osMarca); //ACCedidiendo AL METODO SAVE para guardar todos los datos  
// retorna un bool ---> $res
if ($res){ //si es verdadero  lo redirecciona a  al controlADOR c-marca-list.php
    header("location: c-marca-list.php ");
}else{
    echo "Err 110";
}

?>