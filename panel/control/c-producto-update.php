<?php
session_start();
require_once "../../model/RN_Marca.php";
require_once "../../model/RN_Modelo.php";
require_once "../../model/RN_Rol.php";
require_once "../../model/RN_Usuarios.php";
require_once "../../model/RN_Producto.php";

$u=$_SESSION['id_rol']; 
$s=$_SESSION['usuario']; 
$e=$_SESSION['id']; 
$hash = $_SESSION["hash"];
$oRN_Rol = new RN_Rol;
$listaRol = $oRN_Rol->GetData($u);
$oRN_Usuario = new RN_Usuarios;
$lista_Usuario = $oRN_Usuario->GetData($e);

$osProducto = new Structure_Producto;

$osProducto->hash->SetValue($hash);
$osProducto->descripcion->SetValue( $_POST["Descripcion"] );
$osProducto->precio->SetValue( $_POST["Precio"] );
$osProducto->idMarca->SetValue( $_POST["op_M"] );
$osProducto->idModelo->SetValue( $_POST["op_m"] );
$osProducto->estado->SetValue("Activo");

$oRN_Producto = new RN_Producto;

$res = $oRN_Producto->Update($osProducto);

if ($res){
    header("location: c-producto-list.php");
}else{
    echo "Err-100";
}


?>