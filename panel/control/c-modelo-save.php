<?php

/**
 * @author		Miguel Macias
 * @copyright	2018
 * @company		The Avengers
 * @version		0.1
 */

require_once "../../model/RN_Modelo.php"; //


$Modelo = $_POST["Modelo"]; 


$osModelo = new  Structure_Modelo;  
$osModelo->idModelo->SetValue(0);   
$osModelo->hash->SetValue("");  
$osModelo->nombre->SetValue($Modelo);
$osModelo->estado->SetValue("Activo");

$os_Modelo = new RN_Modelo;  

$res = $os_Modelo->Save($osModelo);  

if ($res){ 
    header("location: c-modelo-list.php ");
}else{
    echo "Err 110";
}

?>