<?php

/**
 * @author		Miguel Macias
 * @copyright	2018
 * @company		The Avengers
 * @version		0.1
 */
session_start();
 
require_once "../../model/RN_Rol.php";
require_once "../../model/RN_Usuarios.php";
require_once "../../model/RN_Producto.php";
require_once "../../model/RN_Marca.php";
require_once "../../model/RN_Modelo.php";
$u=$_SESSION['id_rol']; 
$s=$_SESSION['usuario']; 
$e=$_SESSION['id']; 
$oRN_Rol = new RN_Rol;
$oRN_Usuario = new RN_Usuarios;
$oRN_Producto = new RN_Producto;
$listaRol = $oRN_Rol->GetData($u);
$lista_Usuario = $oRN_Usuario->GetData($e);
$listaProducto =$oRN_Producto->GetList();

include_once "../view/v-producto-list.php";



?>
