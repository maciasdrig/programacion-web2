<?php
session_start();
include '../../model/lib/config.php';


if(isset($_SESSION['usuario']))
{
  //header("Location: index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bienvenido a DRIG</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../view/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../view/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../view/plugins/iCheck/square/blue.css">

  
</head>
<body class="hold-transition login-page" style="background-color:#EEEEEE">
<div class="login-box">
  <div class="login-logo">
 
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Bienvenido </p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Usuario" name="usuario" pattern="[A-Za-z_-0-9]{1,20}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Contraseña" name="contrasena" pattern="[A-Za-z_-0-9]{1,20}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
      <select name ="op" id="inputState" class="form-control">
      <option value="" selected>Seleccionar ...</option>
        <?php
        foreach($listaRol as $item){
          echo "<option name='op' value=". $item->idrol->GetValue() ." >". $item->rol->GetValue() ."</option>";
        } 
        ?>
      </select>
      </div>
    
 
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" name="login" class="btn btn-primary btn-block btn-flat">Iniciar Sesión</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <?php
    if(isset($_POST['login']))

    {

      $usuario = mysql_real_escape_string($_POST['usuario']);
      $usuario = strip_tags($_POST['usuario']);
      $usuario = trim($_POST['usuario']);
      $idrol = mysql_real_escape_string(($_POST['op']));

      $contrasena = mysql_real_escape_string(md5($_POST['contrasena']));
      $contrasena = strip_tags(md5($_POST['contrasena']));
      $contrasena = trim(md5($_POST['contrasena']));

      $query = mysql_query("SELECT * FROM usuarios WHERE usuario = '$usuario' AND contrasena = '$contrasena' AND rol='$idrol'");
      $contar = mysql_num_rows($query);

      if($contar == 1) 

      {

        while($row=mysql_fetch_array($query)) 

        {

          if($usuario = $row['usuario'] && $contrasena = $row['contrasena']&& $idrol=$row['rol'])

          {

            $_SESSION['usuario'] = $usuario;
            $_SESSION['id'] = $row['id_use'];
            $_SESSION['id_rol'] = $row['rol'];

            header('Location: ../control/c-index.php');

          }

        }
        
      } else { echo 'Los datos ingresados no son correctos'; }


    }

    ?>

    <br>

    <a href="#">Olvidé mi contraseña</a><br>
    <a href="c-registrousuario.php" class="text-center">REGISTRESE</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="../view/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../view/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../view/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
