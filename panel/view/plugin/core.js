/** Core **/

$(document).ready(function(){
    
    $(".mnu-ln .item").click(function(){
        ln = $(this).attr("data-ln");
        $.ajax({
                type: 'post',
                url: 'control/x-fn.php',
                data: 'fn=SetLn&ln=' + ln,
                success: function(res){                   
                    cad = res.split("#");
                    
                    for(i=0; i< cad.length; i++){
                        dat = cad[i].split(",")
                        code = dat[0];
                        text = dat[1];
                        
                        $(".lbl").each(function (index){
                            code2 = $(this).attr("data-code");                            
                            if (code == code2){
                                $(this).text(text);
                            }
                       });                        
                    }                    
                    $(".bg-layer").css({"display" : "none"});                    
                }
        });
    })
    
    $(".btn-language").click(function(){
        $(".bg-layer").css({"display" : "block"});
        /*
        $.ajax({
                type: 'post',
                url: 'control/x-fn.php',
                data: 'fn=SetLn&ln=en',
                success: function(res){
                    alert(res);
                    cad = res.split("#");
                    
                    for(i=0; i< cad.length; i++){
                        dat = cad[i].split(",")
                        code = dat[0];
                        text = dat[1];
                        
                        $(".lbl").each(function (index){
                            code2 = $(this).attr("data-code");
                            
                            if (code == code2){
                                $(this).text(text);
                            }
                            
                            
                       });
                        
                    }
                    
                }
        });
        */    
       
    });
    
    $(".ctn-app").click(function(){
        hash = $(this).attr("data-hash");
        
        $("#app").css({"display" : "block"});
        $("#app").animate({"width" : "100%", "height" : "100%", "top" : "0", "left" : "0"}, 1000);   
        setTimeout("OpenApp('"+hash+"')", 1000);
    })
    
})

function OpenApp(hash)
{
    
    
    $.ajax({
            type: 'post',
            url: 'control/x-fn.php',
            data: 'fn=GetNameCtl&hash=' + hash,
            success: function(res){
                $("#content").html(res);
            }
        })
}
function CloseApp()
{
    $("#content").html("");
    $("#app").animate({"width" : "0", "height" : "0", "top" : "50%", "left" : "50%"}, 1000);
    setTimeout("CloseApp2()", 1000);
    
    
}
function CloseApp2()
{
    $("#app").css({"display" : "none"});
}