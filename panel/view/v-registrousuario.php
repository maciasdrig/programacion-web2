<?php
session_start();
include '../../model/lib/config.php';

ini_set('error_reporting',0);

if(isset($_SESSION['usuario']))
{
 // header("Location: index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php //nombre red social ?> Registro</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../view/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../view/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../view/plugins/iCheck/square/blue.css">

</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
  </div>
  <div class="register-box-body">
    <p class="login-box-msg">REGISTRATE</p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" name="nombre" class="form-control" placeholder="Nombre completo" value="<?php echo $_POST['nombre']; ?>" required>
        <span class="glyphicon glyphicon-star form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $_POST['email']; ?>" required>  
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" name="usuario"   class="form-control" placeholder="Usuario" value="<?php echo $_POST['usuario']; ?>" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="contrasena" class="form-control" placeholder="Contraseña" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="repcontrasena" class="form-control" placeholder="Repita la contraseña" required>
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
      <select name ="op" id="inputState" class="form-control">
      <option value="" selected>Seleccionar ...</option>
        <?php
        foreach($listaRol as $item){
          echo "<option name='op' value=". $item->idrol->GetValue() ." >". $item->rol->GetValue() ."</option>";
        } 
        ?>
      </select>
      </div>
      <div class="row">
        <div class="col-xs-10">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="check" required> Acepto los <a href="#">términos y condiciones</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" name="registrar" class="btn btn-primary btn-block btn-flat">Registrarme</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


    <?php
     require_once "../../model/RN_usuarios.php";
    if(isset($_POST['registrar'])) {
      $nombre = mysql_real_escape_string($_POST['nombre']);
      $email = mysql_real_escape_string($_POST['email']);
      $usuario = mysql_real_escape_string($_POST['usuario']);
      $contrasena = mysql_real_escape_string(md5($_POST['contrasena']));
      $repcontrasena = mysql_real_escape_string(md5($_POST['repcontrasena']));
      $idrol = mysql_real_escape_string(($_POST['op']));

      $comprobarusuario = mysql_num_rows(mysql_query("SELECT usuario FROM usuarios WHERE usuario = '$usuario'"));

      $comprobaremail = mysql_num_rows(mysql_query("SELECT email FROM usuarios WHERE email = '$email'"));

      if($comprobarusuario >= 1) { ?>

      <br>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        El nombre de usuario está en uso, por favor escoja otro
     </div>

     <?php } else {

        if($comprobaremail >= 1) { ?>

        <br>
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          El email ya está en uso por favor escoja otro o verifique si tiene una cuenta
        </div>

        <?php } else {

          if($contrasena != $repcontrasena) { ?>

          <br>
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Las contraseñas no coinciden
          </div>

          <?php } 
          else {


             $insertar = mysql_query("INSERT INTO usuarios (hash,nombre,email,usuario,contrasena,fecha_reg,rol,estado) values ('','$nombre','$email','$usuario','$contrasena',now(),'$idrol','activo')");

            if($insertar) { 
              ?>

            <br>
            <div class="alert alert-success alert-dismissible">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              Felicidades se ha registrado correctamente
            </div>

            <?php

            header("Refresh: 2; url = c-login.php");

            }

          }

        }

      }

    }

    ?>

    <br>
   <!-- <a href="login.php" class="text-center">Tengo actualmente una cuenta</a> -->
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 2.2.3 -->
<script src="../view/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../view/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../view/plugins/iCheck/icheck.min.js"></script>


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
