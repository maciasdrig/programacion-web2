


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Bootstrap Admin Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="../view/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../view/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../view/css/plugins/morris.css" rel="stylesheet">

    <!-- los iconos  -->
    <link href="../view/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body style="background-color:#EEEEEE">

    <div id="wrapper"  >

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top "  role="navigation"  style="background-color:#1E221D">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">
                <button style="width:260%;"   type="button" class="btn btn-success btn-sm"  ><?php echo $listaRol->rol->GetValue();?>  </button>
                </a>
                
               
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav ">
              
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $lista_Usuario->usuario->GetValue();?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../control/c-login.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse ">
                <ul class="nav navbar-nav side-nav"  style="background-color:#453D3B">
                    <li class="">
                        <a href="../control/c-index.php" name="x" ><i class="fa fa-fw fa-dashboard"></i> INICIO</a>
    
                    </li>
                    <li  class="text-dark">
                    <a href="../control/c-producto-list.php" ><i class="fa fa-fw fa-table"></i>PRODUCTO</a>
                    </li>
                    <li>
                        <a href="../control/c-marca-list.php"><i class="fa fa-fw fa-table"></i> MARCA</a>
                    </li>
                    <li>
                        <a href="../control/c-modelo-list.php"><i class="fa fa-fw fa-table"></i> MODELO</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
    
        <div id="page-wrapper" style="background-color:#EEEEEE" > 
        <div class="jumbotron jumbotron-fluid">
        <center><h2  >REGISTRO DE USUARIOS</h2></center>
        <div class="container">
        
        <form>
        <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="inputEmail4">Nombre Completo</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Nombre Completo">
                    </div>
                    <div class="form-group col-md-6">
                    <label for="inputPassword4">Email</label>
                    <input type="Email" class="form-control" id="inputPassword4" placeholder="Email">
                    </div>
                </div>

                    <div class="form-row">
                    <div class="form-group col-md-6">
                    <label for="inputEmail4">Usuario</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Usuario">
                    </div>
                    <div class="form-group col-md-6">
                    <label for="inputPassword4">Contraseña</label>
                    <input type="text" class="form-control" id="inputPassword4" placeholder="Contraseña">
                    </div>
                </div>
                    <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputEmail4">Email</label>
                        <select class="form-control form-control-lg" selectd>
                            <option>Seleccionar</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                       
                    </div>
                </div>
                
                <button type="submit" class="btn btn-primary btn-lg ">Guardar</button> 
                <button type="submit" class="btn btn-success btn-lg ">Actualizar</button>
                <button type="submit" class="btn btn-danger btn-lg ">Eliminar</button>
                </form>
        </div>
        </div>


        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../view/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../view/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../view/js/plugins/morris/raphael.min.js"></script>
    <script src="../view/js/plugins/morris/morris.min.js"></script>
    <script src="../view/js/plugins/morris/morris-data.js"></script>

</body>

</html>
