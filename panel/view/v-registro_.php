


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Bootstrap Admin Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="../view/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../view/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../view/css/plugins/morris.css" rel="stylesheet">

    <!-- los iconos  -->
    <link href="../view/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

     <meta http-equiv="content-type" content="text/html" />
    <meta name="author" content="The Avengers" />

    <title>Panel</title>
    <link rel="stylesheet" href="../view/css/main.css" />
    <link rel="stylesheet" href="../view/css/controls.css" />
    
    <script src="../view/plugin/jquery-3.3.1.js"></script>
    <script src="../view/plugin/core.js"></script>
    
    <script>
        $(document).ready(function(){
            $("#btnEdit").click(function(){
                window.location = "c-index.php";
            })
            $("#btnAdd").click(function(){
                window.location = "c-tipotarjeta-new.php";
            })
              $(".btnDelete").click(function(){
                h = $(this).attr("data-hash");
                window.location = "c-tipotarjeta-delete.php?data=" + h;
            })
        });
    </script>

</head>

<body  style="background-color:#EEEEEE">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href=""><?php echo $listaRol->rol->GetValue();?></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $lista_Usuario->usuario->GetValue();?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="c-login.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="../control/c-index.php" name="x" ><i class="fa fa-fw fa-dashboard"></i> INICIO</a>
    
                    </li>
                    <li>
                    <a href="../control/c-producto-list.php" ><i class="fa fa-fw fa-table"></i>PRODUCTO</a>
                    </li>
                    <li>
                        <a href="../control/c-marca-list.php" "><i class="fa fa-fw fa-table"></i> MARCA</a>
                    </li>
                    <li>
                        <a href="   ../control/c-modelo-new.php"><i class="fa fa-fw fa-table"></i> MODELO</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
    
        <div id="page-wrapper"> 
        <div class="jumbotron jumbotron-fluid">
        <center><h2  >REGISTRO DE USUARIOS</h2></center>
        <div class="container">
        <?php


 

 
 $content = "<table border='0' style='width:100%; margin-top:20px;' cellpadding='0' cellspacing='0'>
    <tr class='header'>
        <td width='40'>Nro</td>
        <td width='40'>Nombre</td>
        <td width='30'>Email</td>
        <td width='30'>Contraseña</td>
        
        <td width='150'>Acciones</td>
     
    </tr>";
 
 $sw = true;
 $i = 0;
 foreach($listaTipoTarjeta as $item){
    $i++;
    //echo "<br>" . $item->nombre->GetValue() . " , " . $item->abreviatura->GetValue();
    
    $op =  base64_encode("edit"); 
    $op2 =  base64_encode("delete"); 
    
                    if ($sw){
                        $class = "row-1";
                        $sw = false;
                    }else{
                        $class = "row-2";
                        $sw = true;
                    }
                    
                    $content .= "
                    <tr class='".$class."'>
                        <td>". $i ."</td>
                        <td>". $item->usuario->GetValue() ."</td>
                        <td>". $item->email->GetValue() ."</td>
                        <td>". $item->contrasena->GetValue() ."</td>
                        
                        <td>
                        <button  type='button' class='btn btn-primary btn-sm' id='btnEdit' >Editar</button>
                        <button  type='button' class='btn btn-success btn-sm' >Eliminar</button>
                        </td>
                    </tr>";
                }
                
                $content .= "</table>";

                ?>



    <?php echo $content; ?>




          </div>
        </div>


        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="../view/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../view/js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../view/js/plugins/morris/raphael.min.js"></script>
    <script src="../view/js/plugins/morris/morris.min.js"></script>
    <script src="../view/js/plugins/morris/morris-data.js"></script>

</body>

</html>
